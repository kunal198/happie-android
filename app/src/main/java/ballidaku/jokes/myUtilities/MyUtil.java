package ballidaku.jokes.myUtilities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by brst-pc93 on 7/6/17.
 */

public class MyUtil
{

   static MyUtil instance=new MyUtil();

    public static MyUtil getInstance()
    {
        return instance;

    }


    public static final String TAG = MyUtil.class.getSimpleName();

    public static Toast toast;

    // To show Toast ****************************************************************************************************
    public static void showToast(Context context, String message)
    {
        if (toast != null)
        {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);

        toast.show();

    }


    public void shareText(Context context,String shareTextMsg) {
        try {
            Intent shareIntent = new Intent();
            shareIntent.setAction("android.intent.action.SEND");
            shareIntent.putExtra("android.intent.extra.SUBJECT", "Share via happie");
            shareIntent.putExtra("android.intent.extra.TEXT", shareTextMsg + "\r\n" + "Shared via @gethappieapp Now on #GooglePlay https://goo.gl/Xa4CSu Now on #AppStore https://goo.gl/JdHKOD");
            shareIntent.setType("text/plain");
            context.startActivity(shareIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



   /* public void sharePost(String image_path) {
        try {
            String photoUri = MediaStore.Images.Media.insertImage(this.context.getContentResolver(), image_path, null, null);
            homefile = new File(RealPathUtil.getRealPathFromURI_BelowAPI11(this.context, Uri.parse(photoUri)));
            Intent shareIntent = new Intent();
            shareIntent.setAction("android.intent.action.SEND");
            shareIntent.putExtra("android.intent.extra.TEXT", "Shared via Happie app! Get it on #GooglePlay https://goo.gl/fRLs78 Now on #AppStore https://goo.gl/PNHUwT");
            shareIntent.putExtra("android.intent.extra.STREAM", Uri.parse(photoUri));
            shareIntent.setType("image/jpeg");
            this.context.startActivity(shareIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @SuppressLint({"NewApi"})
    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        String id = DocumentsContract.getDocumentId(uri).split(":")[1];
        String[] column = new String[]{"_data"};
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, "_id=?", new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    @SuppressLint({"NewApi"})
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        Cursor cursor = new CursorLoader(context, contentUri, new String[]{"_data"}, null, null, null).loadInBackground();
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri) {
        Cursor cursor = context.getContentResolver().query(contentUri, new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }*/


    public static Uri getUri(Context context, RelativeLayout view, ImageView view2)
    {

        Bitmap bitmap;
        Uri bmpUri = null;


        if (view.getHeight() > 0 && view.getWidth() > 0)
        {
            Bitmap bmp = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            Drawable bgDrawable = view.getBackground();
            if (bgDrawable != null)
            {
                bgDrawable.draw(canvas);
            }
            else
            {
                canvas.drawColor(-1);
            }
            view.draw(canvas);
            if (view2 != null)
            {
                bitmap = Bitmap.createBitmap(view2.getWidth(), view2.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas1 = new Canvas(bitmap);
                Drawable bgDrawable1 = view2.getBackground();
                if (bgDrawable1 != null)
                {
                    bgDrawable1.draw(canvas1);
                }
                else
                {
                    canvas1.drawColor(-1);
                }
                view2.draw(canvas1);
                bmp = overlay(bmp, bitmap);
            }
            try
            {
                File file1 = new File(context.getCacheDir(), "share_image.jpeg");
                file1.getParentFile().mkdirs();
                FileOutputStream out = new FileOutputStream(file1);
                bmp.compress(Bitmap.CompressFormat.JPEG, 50, out);
                out.close();
                bmpUri = Uri.fromFile(file1);
                //image_path = file1.getAbsolutePath();
            }
            catch (IOException e)
            {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        return bmpUri;
    }


    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2)
    {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight() + bmp2.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0.0f, (float) bmp1.getHeight(), null);
        return bmOverlay;
    }


}
