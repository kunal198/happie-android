package ballidaku.jokes.extraScreens;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import ballidaku.jokes.R;
import ballidaku.jokes.adapters.CommentsAdapter;
import ballidaku.jokes.dataModels.Comments;


public class CommentActivity extends AppCompatActivity
{

    String TAG = CommentActivity.class.getSimpleName();

    Context context;


    RecyclerView recyclerViewComments;

    CommentsAdapter commentsAdapter;

    ArrayList<Comments> commentsArrayList;

    Comments comments;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);


        context =this;

        setUpViews();
    }



    private void setUpViews()
    {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);

        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);


        textViewTitle.setText("Comments");





        recyclerViewComments = (RecyclerView) findViewById(R.id.recyclerViewComments);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewComments.getContext(),DividerItemDecoration.VERTICAL);
        recyclerViewComments.addItemDecoration(dividerItemDecoration);


        commentsArrayList= new ArrayList<>();

        comments =new Comments();
        comments.setUserName("Balli");
        comments.setComment("Nice, comment.");

        commentsArrayList.add(comments);

        comments =new Comments();
        comments.setUserName("Navjot");
        comments.setComment("Hello all, how are you.");
        commentsArrayList.add(comments);

        comments =new Comments();
        comments.setUserName("Balli");
        comments.setComment("Nice, comment.");

        commentsArrayList.add(comments);

        comments =new Comments();
        comments.setUserName("Navjot");
        comments.setComment("I have an ongoing notification and when my activity receives the notification intent, the toggle button should be set to not checked, but it's not working");
        commentsArrayList.add(comments);





        commentsAdapter = new CommentsAdapter(commentsArrayList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewComments.setLayoutManager(mLayoutManager);
        recyclerViewComments.setItemAnimator(new DefaultItemAnimator());
        recyclerViewComments.setAdapter(commentsAdapter);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
