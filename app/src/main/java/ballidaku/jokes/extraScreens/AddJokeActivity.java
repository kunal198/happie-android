package ballidaku.jokes.extraScreens;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import ballidaku.jokes.R;
import ballidaku.jokes.myUtilities.MyConstants;
import ballidaku.jokes.myUtilities.MyUtil;


public class AddJokeActivity extends AppCompatActivity implements View.OnClickListener
{

    String TAG = AddJokeActivity.class.getSimpleName();

    Context context;

    private int PICK_IMAGE_REQUEST = 1;

    private int compressionStatus;

    Bitmap bitmap;


    LinearLayout linearlayoutBothBlock;
    LinearLayout linearLayoutAddTextJoke;


    EditText editTextAddingTextJoke;
    EditText editTextAddJokeDesc;

    ImageView imageViewSetImage;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_joke);


        context = this;

        setUpViews();


    }

    private void setUpViews()
    {
        editTextAddJokeDesc = (EditText) findViewById(R.id.editTextAddJokeDesc);
        editTextAddingTextJoke = (EditText) findViewById(R.id.editTextAddingTextJoke);

        imageViewSetImage = (ImageView) findViewById(R.id.imageViewSetImage);


        linearlayoutBothBlock = (LinearLayout) findViewById(R.id.linearlayoutBothBlock);
        linearLayoutAddTextJoke = (LinearLayout) findViewById(R.id.linearLayoutAddTextJoke);
        findViewById(R.id.linearLayoutTextJoke).setOnClickListener(this);
        findViewById(R.id.linearLayoutUploadImage).setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {

        switch (view.getId())
        {
            case R.id.linearLayoutTextJoke:

                setVisibility(MyConstants.TEXT);

                break;

            case R.id.linearLayoutUploadImage:

                //setVisibility(MyConstants.IMAGE);

                selectImage();

                break;

        }

    }

    public void setVisibility(String type)
    {
        if (linearlayoutBothBlock.getVisibility() == View.VISIBLE && type.equals(MyConstants.TEXT))
        {
            linearlayoutBothBlock.setVisibility(View.GONE);
            linearLayoutAddTextJoke.setVisibility(View.VISIBLE);

            editTextAddJokeDesc.setVisibility(View.GONE);
            editTextAddingTextJoke.setText("");

        }
        else if (linearlayoutBothBlock.getVisibility() == View.VISIBLE && type.equals(MyConstants.IMAGE))
        {
            linearlayoutBothBlock.setVisibility(View.GONE);

            imageViewSetImage.setVisibility(View.VISIBLE);



        }
        else
        {
            linearlayoutBothBlock.setVisibility(View.VISIBLE);
            linearLayoutAddTextJoke.setVisibility(View.GONE);

            editTextAddJokeDesc.setVisibility(View.VISIBLE);
            editTextAddJokeDesc.setText("");

            imageViewSetImage.setImageBitmap(null);
            imageViewSetImage.setVisibility(View.GONE);

        }
    }


    @Override
    public void onBackPressed()
    {
        if (linearlayoutBothBlock.getVisibility() == View.VISIBLE)
        {
            super.onBackPressed();
        }
        else
        {
            setVisibility("Both");
        }
    }


    public void selectImage()
    {


        Intent photoPickerIntent;

        if (Build.VERSION.SDK_INT < 23)
        {
            photoPickerIntent = new Intent("android.intent.action.PICK");
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, PICK_IMAGE_REQUEST);
        }
        else if (Settings.System.canWrite(getApplicationContext()))
        {
            photoPickerIntent = new Intent("android.intent.action.PICK");
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, PICK_IMAGE_REQUEST);
        }
        else
        {
            requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"}, 2909);
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        switch (requestCode)
        {
            case 2909:
                if (grantResults[0] == 0)
                {
                    compressionStatus = 1;

                    Log.e("Permission", "Granted");
                    /*this.jokeType = 1;
                    this.
                    this.mgs = "Choose An Image Joke To Upload";*/
                    Intent photoPickerIntent = new Intent("android.intent.action.PICK");
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, this.PICK_IMAGE_REQUEST);
                    return;
                }
                Log.e("Permission", "Denied");
                return;
            default:
                return;
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        //  this.adding_textjoke.setVisibility(8);
        if (requestCode == this.PICK_IMAGE_REQUEST && resultCode == -1 && data != null && data.getData() != null)
        {
            try
            {
                Uri u = data.getData();
                long length = new File(u.getPath()).length() / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
                if (length > 5000)
                {
                    MyUtil.showToast(context, "Upload failed! The image being uploaded cannot exceed 5MB");
                }
                else if (length < 200)
                {
                    this.compressionStatus = 2;
                    handleCrop(resultCode, data, u);
                }
                else
                {
                    this.compressionStatus = 1;
                    handleCrop(resultCode, data, u);
                }
            }
            catch (Exception e)
            {
                MyUtil.showToast(context,e.getMessage());
                Log.d("Error_msg", e.getMessage());
            }
        }
    }

    private void handleCrop(int resultCode, Intent result, Uri u)
    {
        if (resultCode == -1)
        {
            /*this.text_box.setVisibility(8);
            this.set_image.setVisibility(0);
            this.upload_image.setVisibility(8);
            this.image_text.setVisibility(8);*/

            setVisibility(MyConstants.IMAGE);

            try
            {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(u));


                imageViewSetImage.setImageBitmap(bitmap);

              //  new EditImage().execute(new String[0]);


            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        else if (resultCode == 404)
        {
           // Log.d(String.valueOf(C1610R.string.errorreport), "Error");
        }
        else if (result == null)
        {
           // mgs = "Select Text Joke Or Upload Image";
        }
    }

    private class EditImage extends AsyncTask<String, String, String>
    {
        private EditImage()
        {
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            // AddJokeScreen.swipeRefresh.setRefreshing(true);
        }

        protected String doInBackground(String... params)
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (compressionStatus == 1)
            {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            }
            else if (compressionStatus == 2)
            {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            }
            byte[] b = baos.toByteArray();
            //encoded = Base64Class.encode(b);
            return null;
        }

        protected void onPostExecute(String result)
        {
            //  con = "data:image/jpeg;base64," + encoded;
            /// swipeRefresh.setRefreshing(false);
        }
    }


}
