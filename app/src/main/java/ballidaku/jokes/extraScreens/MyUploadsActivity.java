package ballidaku.jokes.extraScreens;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;

import java.util.ArrayList;

import ballidaku.jokes.R;
import ballidaku.jokes.adapters.MyFeedsAdapter;
import ballidaku.jokes.dataModels.Feeds;


public class MyUploadsActivity extends AppCompatActivity
{
    String TAG =MyUploadsActivity.class.getSimpleName();

    Context context;


    ArrayList<Feeds> studentList;

    UltimateRecyclerView ultimateRecyclerView;
    MyFeedsAdapter myFeedsAdapter;
    LinearLayoutManager linearLayoutManager;
    int moreNum = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_uploads);

        context = this;


        setUpViews();


    }

    private void setUpViews()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);

        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);


        textViewTitle.setText("My Uploads ( 0 )");


        studentList = new ArrayList<Feeds>();

        loadData();


        ultimateRecyclerView = (UltimateRecyclerView) findViewById(R.id.ultimate_recycler_view);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(ultimateRecyclerView.getContext(),DividerItemDecoration.VERTICAL);
        ultimateRecyclerView.addItemDecoration(dividerItemDecoration);

        //ultimateRecyclerView.mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,R.color.colorPrimary,R.color.colorPrimary);
        ultimateRecyclerView.mSwipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.colorPrimary);

        ultimateRecyclerView.setHasFixedSize(false);



        //ultimateRecyclerView.setDefaultSwipeToRefreshColorScheme(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);

        myFeedsAdapter = new MyFeedsAdapter(studentList);

        linearLayoutManager = new LinearLayoutManager(this);
        ultimateRecyclerView.setLayoutManager(linearLayoutManager);
        ultimateRecyclerView.setAdapter(myFeedsAdapter);

        ultimateRecyclerView.disableLoadmore();

        addCustomLoaderView();


        //ultimateRecyclerView.setRecylerViewBackgroundColor(Color.parseColor("#ffffff"));

        swipeRefresh();
        // dragList();
        //infinite_Insertlist();



    }

    // load initial data
    private void loadData()
    {
        /*for (int i = 1; i <= 2; i++)
        {*/
            studentList.add(new Feeds("Student " ,"androidstudent@gmail.com"));

        //}
    }

    public void addCustomLoaderView(){
        myFeedsAdapter.setCustomLoadMoreView(LayoutInflater.from(this)
                                                         .inflate(R.layout.custom_bottom_progressbar, null));
    }


    public  void infinite_Insertlist(){
        ultimateRecyclerView.setOnLoadMoreListener(new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int itemsCount, final int maxLastVisiblePosition) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        myFeedsAdapter.insert(new Feeds("Student " + moreNum++, "androidstudent" +moreNum++ + "@gmail.com"), myFeedsAdapter.getAdapterItemCount());

                    }
                }, 1000);
            }
        });
    }

    public void swipeRefresh(){
        ultimateRecyclerView.setDefaultOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        myFeedsAdapter.insert(new Feeds("Student " + moreNum++, "androidstudent" +moreNum++ + "@gmail.com"), 0);
                        ultimateRecyclerView.setRefreshing(false);
                        linearLayoutManager.scrollToPosition(0);

                    }
                }, 1000);
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
