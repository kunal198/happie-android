package ballidaku.jokes.mainScreens.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;

import ballidaku.jokes.R;
import ballidaku.jokes.extraScreens.AddJokeActivity;
import ballidaku.jokes.extraScreens.MyUploadsActivity;
import ballidaku.jokes.extraScreens.UpDownVotedActivity;
import ballidaku.jokes.frontScreens.LoginSignUpActivity;
import ballidaku.jokes.frontScreens.ProfileActivity;
import ballidaku.jokes.mainScreens.fragments.FeedsFragment;
import ballidaku.jokes.myUtilities.MyConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    String TAG =MainActivity.class.getSimpleName();

    Context context;


    Spinner spinnerCategory;
    Spinner spinnerJokeCategory;

    String[] spinnerCategoryValues = { "Popular", "Latest" };

     DrawerLayout drawer;




    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        setUpViews();

    }

    private void setUpViews()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        spinnerCategory =(Spinner) findViewById(R.id.spinnerCategory);
        spinnerJokeCategory =(Spinner) findViewById(R.id.spinnerJokeCategory);


        findViewById(R.id.imageViewMenu).setOnClickListener(this);

        findViewById(R.id.linearlayoutUploadJoke).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(context,AddJokeActivity.class));
            }
        });


        final LinearLayout bottomBarLinearLayoutHomeScreen =(LinearLayout) findViewById(R.id.bottomBarLinearLayoutHomeScreen);





       // ArrayAdapter adapter = new ArrayAdapter(context,android.R.layout.simple_list_item_1 ,spinnerCategoryValues);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       // spinnerCategory.setAdapter(adapter);




    /*    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle( this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        setUpNavigationViews( headerLayout);

        disableNavigationViewScrollbars(navigationView);


        //toolbar.setNavigationIcon(R.mipmap.ic_menu);
        toolbar.setNavigationIcon(null);



        displayView(0);

    }

    public void openCloseDrawer()
    {
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    private void setUpNavigationViews(View navigationView)
    {
        navigationView.findViewById(R.id.linearlayoutYouUpvoted).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutYouDownvoted).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutMyUploads).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutEditProfile).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutShare).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutRateApp).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutNeedHelp).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutPrivacyPolicy).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutTermsOfService).setOnClickListener(this);
        navigationView.findViewById(R.id.linearlayoutLogIn).setOnClickListener(this);
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        int count = getSupportFragmentManager().getBackStackEntryCount();

       // Log.e("onBackPressedCount", "" + count);


        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(count == 1)
        {
            finish();
        }
        else
        {
            super.onBackPressed();
        }
    }

    public void displayView(int position)
    {

        Fragment myFragment = null;

        switch (position)
        {
            case 0:
                myFragment = new FeedsFragment();
                break;


            /*case 1:
                myFragment = new CheckIn();
                break;

            case 2:
                myFragment = new MyPoints();
                break;


            case 3:
                myFragment = new ShoutOut();
                break;


            case 4:
                myFragment = new ToolBox();
                break;


            case 5:

                myFragment = new ShareWin();
                break;
*/

            default:
                break;
        }


        changeFragment(myFragment);


    }

    private void changeFragment(Fragment fragment)
    {
        if (fragment != null)
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
//            mDrawerLayout.closeDrawers();
        }
    }

    @Override
    public void onClick(View view)
    {

        openCloseDrawer();

        switch (view.getId())
        {

            case R.id.linearlayoutYouUpvoted :

                Intent intent=new Intent(context,UpDownVotedActivity.class);
                intent.putExtra(MyConstants.FROM_WHERE,MyConstants.YOU_UPVOTED);
                startActivity(intent);

                break;

            case R.id.linearlayoutYouDownvoted:


                Intent intent1=new Intent(context,UpDownVotedActivity.class);
                intent1.putExtra(MyConstants.FROM_WHERE,MyConstants.YOU_DOWNVOTED);
                startActivity(intent1);

                break;


            case R.id.linearlayoutMyUploads :

                startActivity(new Intent(context,MyUploadsActivity.class));

                break;

            case R.id.linearlayoutEditProfile :

                startActivity(new Intent(context,ProfileActivity.class));

                break;


            case R.id.linearlayoutShare :

                shareWithFriends();

                break;


            case R.id.linearlayoutRateApp :

                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.happie.jetlabs.happie&hl=en")));

                break;

            case R.id.linearlayoutNeedHelp :

                needHelp();

                break;


            case R.id.linearlayoutPrivacyPolicy :

                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://gethappie.in/privacy-policy.html")));

                break;


            case R.id.linearlayoutTermsOfService :

                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://gethappie.in/terms-of-service.html")));

                break;


            case R.id.linearlayoutLogIn :

                startActivity(new Intent(context,LoginSignUpActivity.class));

                break;

        }

    }



    private void shareWithFriends()
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction("android.intent.action.SEND");
        sendIntent.putExtra("android.intent.extra.TEXT", "Shared via Happie app! Get it on #GooglePlay " + "shareMsgAndroid"/* + " Now on #AppStore " + "shareMsgIphone"*/);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void needHelp()
    {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("mailto:help@happie.in"));
        intent.putExtra("android.intent.extra.SUBJECT", "Feedback");
        intent.putExtra("android.intent.extra.TEXT", "Text Here....");
        startActivity(intent);

    }


}
