package ballidaku.jokes.mainScreens.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ballidaku.jokes.R;


public class YouTubeActivity extends FragmentActivity
{

    YouTubePlayerSupportFragment youTubePlayerFragment;

    String[] abc;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube);



        youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_layout, youTubePlayerFragment).commit();




        youTubePlayerFragment.initialize("AIzaSyBAf3SjwZb4YAomFYzDvS03OM2AZ9JoVkg", new YouTubePlayer.OnInitializedListener()
        {


            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored)
            {
                if (!wasRestored)
                {
                    player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);

                    String a = "https://youtu.be/_-SJ5AqMCBI";
                    // MyUtil.myLog(TAG,"ABC   "+a.substring(a.lastIndexOf("/")+1,a.length()));
                    player.cueVideo(a.substring(a.lastIndexOf("/") + 1, a.length()));


//                    player.play();
                }
            }


            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error)
            {
                // YouTube error
                String errorMessage = error.toString();
                //Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                Log.d("errorMessage:", errorMessage);
            }
        });



/*        youtubeGlobalWebView=(WebView)findViewById(R.id.youtubePlayerWebView);

        youtubeGlobalWebView.setBackgroundColor(Color.parseColor("#000000"));
        youtubeGlobalWebView.setWebChromeClient(new WebChromeClient());
        youtubeGlobalWebView.getSettings().setJavaScriptEnabled(true);
        youtubeGlobalWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        //youtubeGlobalWebView.addJavascriptInterface(new AnonymousClass1WebAppInterface(this.context, holder, joke1), "Android");

        WebSettings webSettings = youtubeGlobalWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setAppCacheMaxSize(8388608);
        webSettings.setAppCacheEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);

        if (Build.VERSION.SDK_INT >= 17) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }
        //youtubeGlobalWebView.loadUrl("file:///android_asset/youtubeVideoPlayer.html?Video=" + extractYTId(joke1.getJokUrl() + "&volumneStatus=0"  *//*+HappieApplication.videoMuteUnmuteState*//*));
        youtubeGlobalWebView.loadUrl("file:///android_asset/youtubeVideoPlayer.html?Video=" + extractYTId("https://youtu.be/_-SJ5AqMCBI" + "&volumneStatus=0"  *//*+HappieApplication.videoMuteUnmuteState*//*));*/

        
        
    }


    public static String extractYTId(String youtubeUrl) {
        String video_id = "";
        if (youtubeUrl == null || youtubeUrl.trim().length() <= 0 || !youtubeUrl.startsWith("http")) {
            return video_id;
        }
        Matcher matcher = Pattern.compile("^.*((youtu.be\\/)|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*", Pattern.CASE_INSENSITIVE).matcher(youtubeUrl);
        if (!matcher.matches()) {
            return video_id;
        }
        String groupIndex1 = matcher.group(7);
        if (groupIndex1 == null || groupIndex1.length() != 11) {
            return video_id;
        }
        return groupIndex1;
    }


   /* class AnonymousClass1WebAppInterface {
        Context mContext;
        final *//* synthetic *//* MyViewHolder val$holder;
        final *//* synthetic *//* Joke_Data_Parcable val$joke1;

        AnonymousClass1WebAppInterface(Context c, MyViewHolder myViewHolder, Joke_Data_Parcable joke_Data_Parcable) {
            this.val$holder = myViewHolder;
            this.val$joke1 = joke_Data_Parcable;
            this.mContext = c;
        }

        @JavascriptInterface
        public void showToast(String State, String State1) {
            if (State.equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
                this.val$holder.iPlayingState = 1;
            } else if (State.equalsIgnoreCase("2")) {
                this.val$holder.iPlayingState = 2;
            } else if (State.equalsIgnoreCase("3")) {
                this.val$holder.iPlayingState = 3;
            } else if (State.equalsIgnoreCase("4")) {
                this.val$holder.iPlayingState = 4;
            } else if (State.equalsIgnoreCase("5")) {
                this.val$holder.iPlayingState = 5;
            }
        }

        @JavascriptInterface
        public void callBackFunctionOfVideo(String videoMuteUnmuteStatus) {
            HappieApplication.videoMuteUnmuteState = Integer.parseInt(videoMuteUnmuteStatus);
        }

        @JavascriptInterface
        public void GetVideoAdvData() {
            if (AdapterHome.this.displayAdvInVideo == 1 && AdapterHome.this.VideoAdvImageURL != null && AdapterHome.this.VideoAdvLogo != null && AdapterHome.this.VideoAdvBrandName != null && AdapterHome.this.VideoAdvBrandURL != null && AdapterHome.this.VideoAdvExtraTxt != null) {
                Handler mHandler = new Handler();
                this.val$holder.youtubePlayerWebView.post(new 1(this));
            }
        }

        @JavascriptInterface
        public void setControllerMuteUnMute(String d) {
            Handler mHandler = new Handler();
            this.val$holder.youtubePlayerWebView.post(new 2(this));
        }

        @JavascriptInterface
        public void openBrandURLToBrower(String URL) {
            Handler mHandler = new Handler();
            this.val$holder.youtubePlayerWebView.post(new 3(this, URL));
        }

        @JavascriptInterface
        public void videoAdvIsDisplayed() {
            videoAdvCount jet_one_happie_commanclass_share_videoAdvCount = new videoAdvCount(String.valueOf(this.val$joke1.getJokId()), String.valueOf(AdapterHome.this.user_id), AdapterHome.this.shareAdvid, AdapterHome.this.context);
        }

        @JavascriptInterface
        public void landscapeWebView(String videoId, String currentTimeOfVideo, String volumneControlerStatus) {
            GoogleAnalytic googleAnalytic;
            if (HappieApplication.MyFirstSelection == 0) {
                googleAnalytic = new GoogleAnalytic(178, AdapterHome.this.context);
            } else if (HappieApplication.MyFirstSelection == 1) {
                googleAnalytic = new GoogleAnalytic(179, AdapterHome.this.context);
            } else if (AdapterHome.this.iPageState == 2) {
                googleAnalytic = new GoogleAnalytic(206, AdapterHome.this.context);
            } else if (AdapterHome.this.iPageState == 3) {
                googleAnalytic = new GoogleAnalytic(209, AdapterHome.this.context);
            }
            LandscapeVideo.sVideoID = videoId;
            LandscapeVideo.sStartTime = currentTimeOfVideo;
            LandscapeVideo.volumneControlerStatusf = volumneControlerStatus;
            LandscapeVideo.iPageStatus = AdapterHome.this.iPageState;
            AdapterHome.this.context.startActivity(new Intent(AdapterHome.this.context, LandscapeVideo.class));
        }
    }*/



}
