package ballidaku.jokes.mainScreens.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;

import java.util.ArrayList;

import ballidaku.jokes.R;
import ballidaku.jokes.adapters.FeedsAdapter;
import ballidaku.jokes.dataModels.Feeds;


public class FeedsFragment extends Fragment
{
    String TAG = FeedsFragment.class.getSimpleName();


    Context context;

    private ArrayList<Feeds> studentList;

    UltimateRecyclerView ultimateRecyclerView;
    FeedsAdapter feedsAdapter;
    LinearLayoutManager linearLayoutManager;
    int moreNum = 2;

    View view;

    public FeedsFragment()
    {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        context = getActivity();

        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_feeds, container, false);

            setUpViews(view);
        }
        return view;
    }

    private void setUpViews(View view)
    {

        studentList = new ArrayList<Feeds>();

        loadData();



        ultimateRecyclerView = (UltimateRecyclerView)view. findViewById(R.id.ultimate_recycler_view);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(ultimateRecyclerView.getContext(),DividerItemDecoration.VERTICAL);
        ultimateRecyclerView.addItemDecoration(dividerItemDecoration);

        //ultimateRecyclerView.mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,R.color.colorPrimary,R.color.colorPrimary);
        ultimateRecyclerView.mSwipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.colorPrimary);

        ultimateRecyclerView.setHasFixedSize(false);



        //ultimateRecyclerView.setDefaultSwipeToRefreshColorScheme(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);

        feedsAdapter = new FeedsAdapter(context,studentList);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        ultimateRecyclerView.setLayoutManager(linearLayoutManager);
        ultimateRecyclerView.setAdapter(feedsAdapter);

        ultimateRecyclerView.enableLoadmore();





        addCustomLoaderView();


        //ultimateRecyclerView.setRecylerViewBackgroundColor(Color.parseColor("#ffffff"));

        swipeRefresh();
        // dragList();
        infinite_Insertlist();




       // ultimateRecyclerView.addOnScrollListener(new abc());

    }


    // load initial data
    private void loadData()
    {
        for (int i = 1; i <= 20; i++)
        {
            studentList.add(new Feeds("Student " + i, "androidstudent" + i + "@gmail.com"));

        }
    }

    public void addCustomLoaderView(){
        feedsAdapter.setCustomLoadMoreView(LayoutInflater.from(getActivity())
                                                         .inflate(R.layout.custom_bottom_progressbar, null));
    }


    public  void infinite_Insertlist(){
        ultimateRecyclerView.setOnLoadMoreListener(new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int itemsCount, final int maxLastVisiblePosition) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        feedsAdapter.insert(new Feeds("Student " + moreNum++, "androidstudent" +moreNum++ + "@gmail.com"), feedsAdapter.getAdapterItemCount());

                    }
                }, 1000);
            }
        });
    }

    public void swipeRefresh(){
        ultimateRecyclerView.setDefaultOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        feedsAdapter.insert(new Feeds("Student " + moreNum++, "androidstudent" +moreNum++ + "@gmail.com"), 0);
                        ultimateRecyclerView.setRefreshing(false);
                        linearLayoutManager.scrollToPosition(0);

                    }
                }, 1000);
            }
        });

    }









}
