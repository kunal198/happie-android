package ballidaku.jokes.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ballidaku.jokes.R;
import ballidaku.jokes.dataModels.Comments;

/**
 * Created by brst-pc93 on 7/7/17.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    private List<Comments> commentsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName, textViewComment;
        ImageView imageViewOwner;

        public MyViewHolder(View view) {
            super(view);
            imageViewOwner = (ImageView) view.findViewById(R.id.imageViewOwner);
            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewComment = (TextView) view.findViewById(R.id.textViewComment);
        }
    }


    public CommentsAdapter(List<Comments> commentsList) {
        this.commentsList = commentsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                                      .inflate(R.layout.custom_comments_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Comments comments = commentsList.get(position);
        holder.textViewName.setText(comments.getUserName());
        holder.textViewComment.setText(comments.getComment());
    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }
}