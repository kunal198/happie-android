package ballidaku.jokes.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;

import java.util.ArrayList;

import ballidaku.jokes.R;
import ballidaku.jokes.dataModels.Feeds;
import ballidaku.jokes.extraScreens.CommentActivity;
import ballidaku.jokes.myUtilities.MyUtil;


public class FeedsAdapter extends UltimateViewAdapter<FeedsAdapter.SimpleAdapterViewHolder>
{

    String TAG = FeedsAdapter.class.getSimpleName();
    private ArrayList<Feeds> stringList;


    private Context context;

    public FeedsAdapter(Context context,ArrayList<Feeds> stringList)
    {
        this.context=context;
        this.stringList = stringList;
    }


    @Override
    public void onBindViewHolder(final SimpleAdapterViewHolder holder, final int position)
    {
        if (position < getItemCount() && (customHeaderView != null ? position <= stringList.size() : position < stringList.size()) && (customHeaderView != null ? position > 0 : true))
        {

            holder.textViewName.setText(stringList.get(position).getName());

            holder.linearlayoutComments.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    context.startActivity(new Intent(context, CommentActivity.class));
                }
            });

            holder.linearLayoutShare.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    //MyUtil.getUri(context,holder.imageViewFeed,)

                    MyUtil.getInstance().shareText(context,"Testing");

                }
            });

        }

    }

    @Override
    public int getAdapterItemCount()
    {
        return stringList.size();
    }

    @Override
    public SimpleAdapterViewHolder getViewHolder(View view)
    {
        return new SimpleAdapterViewHolder(view, false);
    }

    @Override
    public SimpleAdapterViewHolder onCreateViewHolder(ViewGroup parent)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_feeds_row, parent, false);
        SimpleAdapterViewHolder vh = new SimpleAdapterViewHolder(v, true);
        return vh;
    }


    public void insert(Feeds feeds, int position)
    {
        insertInternal(stringList, feeds, position);
    }

    public void remove(int position)
    {
        removeInternal(stringList, position);
    }

    public void clear()
    {
        clearInternal(stringList);
    }

    @Override
    public void toggleSelection(int pos)
    {
        super.toggleSelection(pos);
    }

    @Override
    public void setSelected(int pos)
    {
        super.setSelected(pos);
    }

    @Override
    public void clearSelection(int pos)
    {
        super.clearSelection(pos);
    }


    public void swapPositions(int from, int to)
    {
        swapPositions(stringList, from, to);
    }


    @Override
    public long generateHeaderId(int position)
    {

        if (getItem(position).length() > 0)
            return getItem(position).charAt(0);
        else return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup)
    {
        //View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stick_header_item, viewGroup, false);
        //return new RecyclerView.ViewHolder(view)
        return new RecyclerView.ViewHolder(null)
        {
        };
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int position)
    {

//        TextView textView = (TextView) viewHolder.itemView.findViewById(R.id.stick_text);
//        textView.setText(String.valueOf(getItem(position).charAt(0)));
//        viewHolder.itemView.setBackgroundColor(Color.parseColor("#AAffffff"));


    }

    @Override
    public void onItemMove(int fromPosition, int toPosition)
    {
        swapPositions(fromPosition, toPosition);
//        notifyItemMoved(fromPosition, toPosition);
        super.onItemMove(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position)
    {
        if (position > 0)
            remove(position);
        // notifyItemRemoved(position);
//        notifyDataSetChanged();
        super.onItemDismiss(position);
    }


    public void setOnDragStartListener(OnStartDragListener dragStartListener)
    {
        mDragStartListener = dragStartListener;

    }


    public class SimpleAdapterViewHolder extends UltimateRecyclerviewViewHolder
    {

        TextView textViewName;

        LinearLayout linearlayoutComments;
        LinearLayout linearLayoutShare;

        ImageView imageViewFeed;


//        ProgressBar progressBarSample;
       // View item_view;

        public SimpleAdapterViewHolder(View itemView, boolean isItem)
        {
            super(itemView);

            if (isItem)
            {
                textViewName = (TextView) itemView.findViewById( R.id.textViewName);

                linearlayoutComments = (LinearLayout) itemView.findViewById(R.id.linearlayoutComments);
                linearLayoutShare = (LinearLayout) itemView.findViewById(R.id.linearLayoutShare);

                imageViewFeed = (ImageView) itemView.findViewById(R.id.imageViewFeed);
               /*
                progressBarSample = (ProgressBar) itemView.findViewById(R.id.progressbar);
                progressBarSample.setVisibility(View.GONE);
                item_view = itemView.findViewById(R.id.itemview);*/




            }

        }

        @Override
        public void onItemSelected()
        {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear()
        {
            itemView.setBackgroundColor(0);
        }
    }

    public String getItem(int position)
    {
        if (customHeaderView != null)
            position--;
        if (position < stringList.size())
            return stringList.get(position).getName();
        else return "";
    }

}