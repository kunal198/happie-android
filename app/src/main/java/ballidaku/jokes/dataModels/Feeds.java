package ballidaku.jokes.dataModels;

import java.io.Serializable;

/**
 * Created by brst-pc93 on 7/3/17.
 */

public class Feeds implements Serializable
{

    private static final long serialVersionUID = 1L;

    private String name;

    private String emailId;

    public Feeds() {

    }
    public Feeds(String name, String emailId) {
        this.name = name;
        this.emailId = emailId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }


}
